const {
    gitDescribeSync
} = require('git-describe');
const {
    version
} = require('./package.json');
const {
    resolve,
    relative
} = require('path');
const {
    writeFileSync
} = require('fs-extra');
const git = require('git-rev-sync');



const gitInfo = gitDescribeSync({
    requireAnnotated: true
});
gitInfo.version = version;

gitInfo.tag = git.tag();
console.log(gitInfo.tag)
const file = resolve(__dirname, 'src', 'environments', 'version.ts');
writeFileSync(file,
    `// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
    /* tslint:disable */
    export const VERSION = ${JSON.stringify(gitInfo, null, 2)};
    /* tslint:enable */
    `, {
        encoding: 'utf-8'
    });

console.log(`Wrote version info ${gitInfo.tag} rev ${gitInfo.hash} to ${relative(resolve(__dirname, '..'), file)}`);