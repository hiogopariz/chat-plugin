(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = global || self, factory(global.chat = {}));
}(this, function (exports) { 'use strict';

  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
      var info = gen[key](arg);
      var value = info.value;
    } catch (error) {
      reject(error);
      return;
    }

    if (info.done) {
      resolve(value);
    } else {
      Promise.resolve(value).then(_next, _throw);
    }
  }

  function _asyncToGenerator(fn) {
    return function () {
      var self = this,
          args = arguments;
      return new Promise(function (resolve, reject) {
        var gen = fn.apply(self, args);

        function _next(value) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
        }

        function _throw(err) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
        }

        _next(undefined);
      });
    };
  }

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);

    if (Object.getOwnPropertySymbols) {
      var symbols = Object.getOwnPropertySymbols(object);
      if (enumerableOnly) symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
      keys.push.apply(keys, symbols);
    }

    return keys;
  }

  function _objectSpread2(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};

      if (i % 2) {
        ownKeys(source, true).forEach(function (key) {
          _defineProperty(target, key, source[key]);
        });
      } else if (Object.getOwnPropertyDescriptors) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
      } else {
        ownKeys(source).forEach(function (key) {
          Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
        });
      }
    }

    return target;
  }

  var axios = require('axios');

  var $websocket;
  var $config = {
    HEADERS: null,
    HTTP_URL: null,
    WS_URL: null,
    USER: null,
    CONNECTION: 'DISCONNECTED',
    onconnection: null
  };
  var $events = {}; // lista de eventos

  var $axios;
  var $storage = {};
  /**
   * adiciona um listener de evento
   * @param {string} event evento para o listener
   * @param {Function} listener listener para receber os parametros do evento
   */

  function on(event, listener) {
    // adiciona listenners de eventos
    if (!$events[event]) {
      $events[event] = {
        listeners: []
      };
    }

    $events[event].listeners.push(listener);
  }
  /**
   * deleta um listener de evento
   * @param {string} event indicador do evento a ser deletado
   */

  function off(event) {
    // remove listeners
    delete $events[event];
  }
  /**
   * Aciona um listener de evento
   * @param {string} name Nome do evento a ser emitido
   * @param  {...any} payload Payload do evento
   */

  function emit(name) {
    if ($events[name]) {
      for (var _len = arguments.length, payload = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        payload[_key - 1] = arguments[_key];
      }

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = $events[name].listeners[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var listener = _step.value;
          listener.apply(this, payload);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }
  }
  /**
   * Atualiza o storage
   */


  function updateStorage() {
    window.localStorage.setItem('chatStorage', JSON.stringify($storage));
  }
  /**
   * @returns {object} retorna o objeto salvo no storage
   */


  function getStorage() {
    var storage = window.localStorage.getItem('chatStorage');
    return JSON.parse(storage);
  }
  /**
   * Adiciona as configurações do chat para conexão com o server
   * @param {object} config objeto de configuração
   * @param {string} config.HTTP_URL url padrão para as requisições
   * @param {string} config.WS_URL url padrão para conexão com o WebSocket
   * @param {string} config.HEADERS header para tentar a conexão
   * @param {object} config.USER usuário que logou no chat
   * @param {function} config.onconnection listener de conexão @param {string} CONNECTION quando fica online recebe "CONNECTED" e offline "DISCONNECTED"
   */


  function setConfig(config) {
    $config = _objectSpread2({}, $config, {}, config);
    var headers = typeof $config.HEADERS === 'string' ? [$config.HEADERS] : $config.HEADERS;
    $axios = axios.create({
      baseURL: $config.HTTP_URL,
      headers: headers
    });

    window.ononline = function () {
      $config.CONNECTION = "CONNECTED";

      if ($config.onconnection) {
        $config.onconnection($config.CONNECTION);
      }
    };

    window.offline = function () {
      $config.CONNECTION = "DISCONNECTED";

      if ($config.onconnection) {
        $config.onconnection($config.CONNECTION);
      }
    };
  }
  /**
   * abre conexão com o websocket
   */

  function connect() {
    $websocket = new WebSocket($config.WS_URL);

    $websocket.onopen = function () {
      $config.CONNECTION = 'CONNECTED';

      if ($config.onconnection) {
        $config.onconnection($config.CONNECTION);
      }

      $websocket.send(JSON.stringify({
        status: $config.CONNECTION,
        user: $config.USER
      }));
      $storage['user'] = $config.USER;
      updateStorage();
    };

    $websocket.onmessage = function (event) {
      var data = JSON.parse(event.data);

      switch (data.status) {
        case 'MESSAGE':
          {
            if ($events.message && data) {
              if (data.file) {
                emit('file', data);
              } else {
                emit('message', data);
              }
            }
          }

        case 'STATUS':
          {
            if ($events.status && data.status && data.user) {
              emit('status', data);
            }
          }

        case 'TYPING':
          {
            if ($events.typing && data.typing && data.user) {
              emit('typing', data);
            }
          }
      }
    };
  }
  /**
   * Método para enviar mensagem
   * @param {string} message string da mensagem
   * @param {object} user usuário que enviou a mensagem
   * @param {object} conversa conversa de destino da mensagem
   */

  function sendMessage(message, user, conversa) {
    if ($websocket instanceof WebSocket && $config.CONNECTION === 'CONNECTED') {
      var _message = {
        user: user,
        message: message,
        conversa: conversa,
        timestamp: new Date().getTime(),
        status: 'MESSAGE'
      };
      $websocket.send(JSON.stringify(_message));
    }
  }
  /**
   * Método para enviar arquivos
   * @param {File} file arquivo a ser enviado
   * @param {object} user usuário que enviou a mensagem
   * @param {object} conversa conversa de destino da mensagem
   */

  function sendFile(file, user, conversa) {
    if ($websocket instanceof WebSocket && $config.CONNECTION === 'CONNECTED') {
      var mimeType = file.type;
      var reader = new FileReader();

      reader.onload = function () {
        var _message = {
          user: user,
          file: reader.result,
          conversa: conversa,
          timestamp: new Date().getTime(),
          status: 'MESSAGE',
          mimeType: mimeType
        };
        $websocket.send(JSON.stringify(_message));
      };

      reader.onerror = function (error) {
        return console.warn('⚠ Não foi possível enviar o arquivo.', error);
      };

      reader.readAsDataURL(file);
    }
  }
  /**
   * Método para atualizar o status de "digitando" do usuário na conversa
   * @param {boolean} typing está digitando ou parou de digitar
   * @param {object} user usuário que está digitando
   * @param {object} conversa conversa que o usuário está digitando
   */

  function typing(typing, user, conversa) {
    if ($websocket instanceof WebSocket && $config.CONNECTION === 'CONNECTED') {
      var _typing = {
        user: user,
        conversa: conversa,
        typing: typing,
        status: 'TYPING'
      };
      $websocket.send(JSON.stringify(_typing));
    }
  }
  /**
   * Método para alterar o status do usuário (online/offline/busy)
   * @param {string} status novo status do usuário
   * @param {object} user usuário em que o status foi alterado
   * @param {array} contatos lista de contatos do usuário
   */

  function changeStatus(status, user, contatos) {
    if ($websocket instanceof WebSocket && $config.CONNECTION === 'CONNECTED') {
      var _status = _defineProperty({
        user: user,
        status: status,
        contatos: contatos
      }, "status", 'STATUS');

      $websocket.send(JSON.stringify(_status));
    }
  }
  /**
   * Método que retorna a lista de contatos
   * @returns {array} Array com os contatos
   */

  function getContatos() {
    return _getContatos.apply(this, arguments);
  }
  /**
   * Método que retorna a lista de conversas
   * @returns {Array} Array com as conversas
   */

  function _getContatos() {
    _getContatos = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var response, contatos, storage;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;

              if (!window.navigator.onLine) {
                _context.next = 11;
                break;
              }

              _context.next = 4;
              return $axios.get('/chat/contatos');

            case 4:
              response = _context.sent;
              contatos = response.data;

              if (Array.isArray(contatos)) {
                $storage['contatos'] = contatos;
              }

              updateStorage();
              return _context.abrupt("return", contatos);

            case 11:
              storage = getStorage();
              return _context.abrupt("return", storage.contatos);

            case 13:
              _context.next = 18;
              break;

            case 15:
              _context.prev = 15;
              _context.t0 = _context["catch"](0);
              console.log('⚠ Erro ao recuperar a lista de contatos', _context.t0);

            case 18:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 15]]);
    }));
    return _getContatos.apply(this, arguments);
  }

  function getConversas() {
    return _getConversas.apply(this, arguments);
  }
  /**
   * Método para adicionar um contato
   * @param {object} contato Contato a ser adicionado
   * @returns {array} retorna novo array de contatos
   */

  function _getConversas() {
    _getConversas = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2() {
      var response, conversas, storage;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.prev = 0;

              if (!window.navigator.onLine) {
                _context2.next = 12;
                break;
              }

              _context2.next = 4;
              return $axios.get('/chat/conversas');

            case 4:
              response = _context2.sent;
              conversas = response.data;
              $storage['conversas'] = conversas;

              if (Array.isArray(conversas)) {
                $storage.conversas.forEach(function (conversa) {
                  if (conversa.mensagens) conversa.mensagens = conversa.mensagens.slice(0, 10);
                });
              }

              updateStorage();
              return _context2.abrupt("return", conversas);

            case 12:
              storage = getStorage();
              return _context2.abrupt("return", storage.conversas);

            case 14:
              _context2.next = 19;
              break;

            case 16:
              _context2.prev = 16;
              _context2.t0 = _context2["catch"](0);
              console.log('⚠ Erro ao recuperar a lista de conversas', _context2.t0);

            case 19:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[0, 16]]);
    }));
    return _getConversas.apply(this, arguments);
  }

  function addContato(_x) {
    return _addContato.apply(this, arguments);
  }
  /**
   * Método para criar uma conversa
   * @param {object} contato Contato a começar uma conversa
   * @returns {array} retona novo array de conversas
   */

  function _addContato() {
    _addContato = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3(contato) {
      var response;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.prev = 0;
              _context3.next = 3;
              return $axios.post('/chat/contatos', contato);

            case 3:
              response = _context3.sent;
              return _context3.abrupt("return", response.data);

            case 7:
              _context3.prev = 7;
              _context3.t0 = _context3["catch"](0);
              console.log('⚠ Erro ao adicionar contato', _context3.t0);

            case 10:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, null, [[0, 7]]);
    }));
    return _addContato.apply(this, arguments);
  }

  function addConversa(_x2) {
    return _addConversa.apply(this, arguments);
  }
  /**
   * Método para remover um contato da lista
   * @param {object} contato Contato a ser excluído
   * @returns {array} retorna novo array de contatos
   */

  function _addConversa() {
    _addConversa = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee4(contato) {
      var response;
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.prev = 0;
              _context4.next = 3;
              return $axios.post('/chat/conversas', contato);

            case 3:
              response = _context4.sent;
              return _context4.abrupt("return", response.data);

            case 7:
              _context4.prev = 7;
              _context4.t0 = _context4["catch"](0);
              console.log('⚠ Erro ao adicionar conversa', _context4.t0);

            case 10:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, null, [[0, 7]]);
    }));
    return _addConversa.apply(this, arguments);
  }

  function removeContato(_x3) {
    return _removeContato.apply(this, arguments);
  }
  /**
   * Método para remover uma conversa
   * @param {object} conversa Conversa a ser excluída
   * @returns {array} retona novo array de conversas
   */

  function _removeContato() {
    _removeContato = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee5(contato) {
      var response;
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.prev = 0;
              _context5.next = 3;
              return $axios.delete('/chat/contatos/' + contato.id);

            case 3:
              response = _context5.sent;
              return _context5.abrupt("return", response.data);

            case 7:
              _context5.prev = 7;
              _context5.t0 = _context5["catch"](0);
              console.log('⚠ Erro ao remover contato', _context5.t0);

            case 10:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, null, [[0, 7]]);
    }));
    return _removeContato.apply(this, arguments);
  }

  function removeConversa(_x4) {
    return _removeConversa.apply(this, arguments);
  }
  /**
   * fecha conexão com o websocket
   */

  function _removeConversa() {
    _removeConversa = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee6(conversa) {
      var response;
      return regeneratorRuntime.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              _context6.prev = 0;
              _context6.next = 3;
              return $axios.delete('/chat/conversas/' + conversa.id);

            case 3:
              response = _context6.sent;
              return _context6.abrupt("return", response.data);

            case 7:
              _context6.prev = 7;
              _context6.t0 = _context6["catch"](0);
              console.log('⚠ Erro ao remover conversa', e);

            case 10:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee6, null, [[0, 7]]);
    }));
    return _removeConversa.apply(this, arguments);
  }

  function disconnect() {
    if ($websocket instanceof WebSocket) {
      $config.CONNECTION = 'DISCONNECTED';

      if ($config.onconnection) {
        $config.onconnection($config.CONNECTION);
      }

      $websocket.send(JSON.stringify({
        status: $config.CONNECTION,
        user: $config.USER
      }));
      window.localStorage.clear();
      $websocket.close();
    }
  }
  /**
   * @returns {object} retorna o objeto de configuração
   */

  function getConfig() {
    return $config;
  }

  exports.addContato = addContato;
  exports.addConversa = addConversa;
  exports.changeStatus = changeStatus;
  exports.connect = connect;
  exports.disconnect = disconnect;
  exports.getConfig = getConfig;
  exports.getContatos = getContatos;
  exports.getConversas = getConversas;
  exports.off = off;
  exports.on = on;
  exports.removeContato = removeContato;
  exports.removeConversa = removeConversa;
  exports.sendFile = sendFile;
  exports.sendMessage = sendMessage;
  exports.setConfig = setConfig;
  exports.typing = typing;

  Object.defineProperty(exports, '__esModule', { value: true });

}));
