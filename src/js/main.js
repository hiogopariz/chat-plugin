require('../scss/main.scss');
const Chat = require('./chat.umd');
const {
    name,
    wsUrl
} = require('../environments/consts');

console.log(process.env.NODE_ENV);

const layout = `
<button id="buttonChat" class="chat_plugin_button"></button>
<div id="chatBody" class="chat_plugin_body">
    <div class="chat_plugin_header">
        <div class="chat_plugin_image_header"></div>
        <span>${name}</span>
        <button id="chatCloseBtn" class="chat_plugin_close_btn">&times;</button>
    </div>
    <div id="chatMessagesHolder" class="chat_plugin_messages_holder">
    </div>
    <form id="chatForm" class="chat_plugin_form" autocomplete="off">
        <input id="chatText" name="chatText" type="text" autocomplete="off"/>
        <label class="chat_plugin_attach_button">
            <input type="file" id="attachInput" />
            <svg aria-hidden="true" focusable="false" width="21" height="18" data-prefix="fas" data-icon="paperclip" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="white" d="M43.246 466.142c-58.43-60.289-57.341-157.511 1.386-217.581L254.392 34c44.316-45.332 116.351-45.336 160.671 0 43.89 44.894 43.943 117.329 0 162.276L232.214 383.128c-29.855 30.537-78.633 30.111-107.982-.998-28.275-29.97-27.368-77.473 1.452-106.953l143.743-146.835c6.182-6.314 16.312-6.422 22.626-.241l22.861 22.379c6.315 6.182 6.422 16.312.241 22.626L171.427 319.927c-4.932 5.045-5.236 13.428-.648 18.292 4.372 4.634 11.245 4.711 15.688.165l182.849-186.851c19.613-20.062 19.613-52.725-.011-72.798-19.189-19.627-49.957-19.637-69.154 0L90.39 293.295c-34.763 35.56-35.299 93.12-1.191 128.313 34.01 35.093 88.985 35.137 123.058.286l172.06-175.999c6.177-6.319 16.307-6.433 22.626-.256l22.877 22.364c6.319 6.177 6.434 16.307.256 22.626l-172.06 175.998c-59.576 60.938-155.943 60.216-214.77-.485z"></path></svg>
        </label>
        <button type="submit" class="chat_plugin_submit_button">
            <svg xmlns="http://www.w3.org/2000/svg" width="21" height="18" viewBox="0 0 21 18"><path fill="#FFF" d="M2.01,21,23,12,2.01,3,2,10l15,2L2,14Z" transform="translate(-2 -3)"/></svg>
        </button>
    </form>
</div>
`

function urltoFile(url) {
    const mimeType = (url.match(/^data:([^;]+);/) || '')[1];
    return (fetch(url)
        .then(function (res) {
            return res.arrayBuffer();
        })
        .then(function (buf) {
            return new File([buf], 'teste', {
                type: mimeType
            });
        })
    );
}

const files = new Array();

window.openFile = (index) => {
    const url = window.URL.createObjectURL(files[index]);
    window.open(url, 'new');
}

const chatConfig = {
    WS_URL: wsUrl,
    onconnection: function (connection) {
        if (connection === 'CONNECTED') {

            const chatDiv = document.createElement('div');
            chatDiv.setAttribute('id', 'chatHolder');
            chatDiv.classList.add('chat_plugin_holder');
            chatDiv.innerHTML = layout;
            document.body.appendChild(chatDiv);
            const button = document.getElementById('buttonChat');
            const chat = document.getElementById('chatBody');
            const closeBtn = document.getElementById('chatCloseBtn');
            const messageHolder = document.getElementById('chatMessagesHolder');
            const form = document.getElementById('chatForm');
            const chatText = document.getElementById('chatText');
            const attachInput = document.getElementById('attachInput');

            function scrollToBottom() {
                messageHolder.scrollTo(0, messageHolder.scrollHeight);
            }

            button.addEventListener('click', (e) => {
                chat.classList.toggle('chat_plugin_body-opened');
                chatText.focus();
                scrollToBottom();
            })

            closeBtn.addEventListener('click', (e) => {
                chat.classList.remove('chat_plugin_body-opened');
            })

            function addMessageByMe(message) {
                const messageByMe = document.createElement('div');
                messageByMe.className = "chat_plugin_message chat_plugin_message_by_me";
                messageByMe.innerHTML = `<span>${message}</span>`;
                messageHolder.appendChild(messageByMe);
                scrollToBottom();
            }

            form.addEventListener('submit', (e) => {
                e.preventDefault();
                if (chatText.value && chatText.value.length > 0) {
                    Chat.sendMessage(chatText.value);
                    addMessageByMe(chatText.value);
                    chatText.value = '';
                    scrollToBottom();
                }
            })

            attachInput.addEventListener('change', (event) => {
                console.log('changed');
                for (let file of event.target.files) {
                    Chat.sendFile(file);
                    console.log(file.size);
                    if (file.size > 2097152) {
                        const reader = new FileReader();
                        reader.onload = () => {
                            const messageByMe = document.createElement('div');
                            messageByMe.classList.add("chat_plugin_message");
                            messageByMe.classList.add("chat_plugin_message_by_me");
                            messageByMe.classList.add("chat_plugin_message_image");
                            const image = `<img src="${reader.result}" alt="${file.name}">`;
                            messageByMe.innerHTML = image;
                            messageHolder.appendChild(messageByMe);
                        }
                    }
                    reader.readAsDataURL(file);
                }
            })

            function addMessageReceived(data) {
                const messageReceived = document.createElement('div');
                messageReceived.className = "chat_plugin_message chat_plugin_message_received";
                messageReceived.innerHTML = `<span>${data.message}</span>`;
                messageHolder.appendChild(messageReceived);
                scrollToBottom();
            }

            function fileReceived(data) {
                files.push(data.file);
                urltoFile(data.file).then(file => {
                    const index = files.length;
                    const messageReceived = document.createElement('div');
                    if (file.type.includes('image')) {
                        const image = `<img src="${data.file}" alt="${file.name}">`;
                        messageReceived.classList.add("chat_plugin_message");
                        messageReceived.classList.add("chat_plugin_message_received");
                        messageReceived.classList.add("chat_plugin_message_image");
                        messageReceived.innerHTML = image;
                    } else {
                        const link = `<span onClick="openFile(${index})">${file.name}</span>`
                        messageReceived.classList.add("chat_plugin_message");
                        messageReceived.classList.add("chat_plugin_message_received");
                        messageReceived.classList.add("chat_plugin_message_link");
                        messageReceived.innerHTML = link;
                    }
                    messageHolder.appendChild(messageReceived);
                    scrollToBottom();
                });
            }

            Chat.on('message', addMessageReceived);
            Chat.on('file', fileReceived);
        }
    }
}
Chat.setConfig(chatConfig);
Chat.connect();