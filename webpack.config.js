const path = require("path")
const ImageminPlugin = require('imagemin-webpack-plugin').default
const imageminMozjpeg = require('imagemin-mozjpeg')
const HtmlMinifierPlugin = require('html-minifier-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const WebpackMd5Hash = require('webpack-md5-hash')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const webpack = require('webpack')
const dev = require('./src/environments/environment.dev');
const hom = require('./src/environments/environment.hom');
const prod = require('./src/environments/environment.prod');

module.exports = (env, argv) => {
    let wsUrl, name, mode;

    console.log(argv);

    switch(argv.mode) {
        case 'development': {
            wsUrl = dev[env].url;
            name = dev[env].name;
            mode="dev"
            break;
        }
        case 'production': {
            wsUrl = prod[env].url;
            name = prod[env].name;
            mode="prod"
            break;
        }
        default: {
            wsUrl = hom[env].url;
            name = hom[env].name;
            mode="hom"
        }
    }

    return {
        "entry": ["./src/js/main.js"],
        "output": {
            path: path.resolve(__dirname, './dist'),
            filename: `./${env}.js`,
            publicPath: '/'
        },
        devtool: 'inline-source-map',
        devServer: {
            contentBase: false,
            open: true,
            overlay: {
                warnings: false,
                errors: true,
                quiet: true,
            }
        },
        module: {
            rules: [
                {
                    test: /\.m?js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env"]
                        }
                    }
                },
                {
                    test: /\.scss$/,
                    use: [
                        'style-loader',
                        'css-loader',
                        'postcss-loader',
                        'sass-loader'
                    ]
                },
                {
                    test: /\.(png|jp(e*)g|svg)$/,
                    use: [{
                        loader: 'url-loader',
                    }]
                },
                {
                    test: /\.(ttf|otf|woff|woff2)$/,
                    use: [{
                        loader: 'url-loader',
                    }],
                },
            ]
        },
        "plugins": [
            new CleanWebpackPlugin("./dist", {}),
            new ImageminPlugin({
                test: /\.(jpe?g|png|gif|svg)$/i,
                optipng: {
                    "optimizationLevel": 9
                },
                plugins: [
                    imageminMozjpeg({
                        quality: 80,
                        progresive: true
                    })
                ]
            }),
            new webpack.DefinePlugin({
                '__URL__': JSON.stringify(wsUrl),
                '__NAME__': JSON.stringify(name),
            })
        ]
    }
}